"""
Project Euler Problem 4

A palindromic number reads the same both ways. The largest palindrome made from the
product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.


Uses a while loop to track two numbers and its products to see which palindrome
product are the biggest.
"""


__author__ = 'Ben'


if __name__ == '__main__':
    # Initial numbers, they are the smallest numbers that are 3 digits.
    first_number = 100
    second_number = 100

    # A list to store the largest palindrome found, the first two entries are the numbers
    # that create the palindrome. The palindrome itself is stored in the third entry.
    largest_palindrome = [0, 0, 0]

    while len(str(first_number)) <= 3:
        if len(str(second_number)) > 3:
            # This is the condition to restart the while loop with a bigger first_number
            # since at this point we exhausted the combinations between the first_number
            # and all 3 digit values for the second_number
            first_number += 1
            second_number = 100
            continue

        product = first_number * second_number

        # Checks if the product and the product reversed are the same
        if (str(product) == str(product)[::-1]) is True:
            if product > largest_palindrome[2]:
                # If current palindrome is larger than the one in the list,
                # store the new palindrome.
                largest_palindrome[0] = first_number
                largest_palindrome[1] = second_number
                largest_palindrome[2] = product

        second_number += 1

    print(largest_palindrome)